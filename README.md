### How do I get set up? ###
- Copy .env.example into .env and set some of important parameter related with MySQL database
```
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

```

- Run migration together with seeder
```
php artisan migrate --seed
```

- If you doesn't have any apache or nginx setup on your local, you could server your project locally through this command
```
php -S localhost:8000 -t public
```

- To test the API, you could use postman via this button bellow

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/04f971981b4563b764be#?env%5BLocal%5D=W3sia2V5IjoiaG9zdCIsInZhbHVlIjoiaHR0cDovL3Rlc3QubG9jYWxob3N0IiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJwYXRoIiwidmFsdWUiOiIvYXBpIiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJ2ZXJzaW9uIiwidmFsdWUiOiIvdjEiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InRva2VuX2FkbWluIiwidmFsdWUiOiIyMTIzMmYyOTdhNTdhNWE3NDM4OTRhMGU0YTgwMWZjMyIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoidG9rZW5fcHVibGljIiwidmFsdWUiOiI4YTVkYTUyZWQxMjY0NDdkMzU5ZTcwYzA1NzIxYThhYSIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoicHJvZHVjdF9pZCIsInZhbHVlIjoiZjJhZjViNDAxNTVhNDgxY2I1ZWEwNTVkMWViODVkMjAiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6Im9yZGVyX2lkIiwidmFsdWUiOiIyNzFiMWU1YTZjMGY0NmRhYWIyMDQ5ZmEzMTQ5NDViYSIsImVuYWJsZWQiOnRydWV9XQ==)

- To execute the queue properly, you could setup beanstalkd, redis or any preferable queue driver on your local and then update .env value related with queue
```
QUEUE_CONNECTION=
```
- Don't forget to listen the queue from Laravel cli
```
php artisan queue:listen --tries=1
```
