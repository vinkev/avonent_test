<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['namespace' => 'Api', 'prefix' => 'api', 'as' => 'api', 'middleware' => ['json.response']], function () use ($router) {
    $router->group(['namespace' => 'V1', 'prefix' => 'v1', 'as' => 'v1', 'middleware' => ['auth']], function () use ($router) {

        $router->group(['prefix' => 'product'], function () use ($router) {
            $router->get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
            $router->post('/', ['as' => 'store', 'uses' => 'ProductController@store']);
            $router->get('/{productId}', ['as' => 'show', 'uses' => 'ProductController@show']);
            $router->put('/{productId}', ['as' => 'update', 'uses' => 'ProductController@update']);
            $router->delete('/{productId}', ['as' => 'destroy', 'uses' => 'ProductController@destroy']);
        });

        $router->group(['prefix' => 'order'], function () use ($router) {
            $router->get('/', ['as' => 'index', 'uses' => 'OrderController@index']);
            $router->post('/', ['as' => 'store', 'uses' => 'OrderController@store']);
            $router->get('/{orderId}', ['as' => 'show', 'uses' => 'OrderController@show']);
            $router->put('/{orderId}', ['as' => 'update', 'uses' => 'OrderController@update']);
            $router->delete('/{orderId}', ['as' => 'destroy', 'uses' => 'OrderController@destroy']);
        });
    });
});
