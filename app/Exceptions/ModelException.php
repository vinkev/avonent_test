<?php
/**
 * test - ModelException.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 12.52
 */

namespace App\Exceptions;


use App\Helpers\Classes\RestResponse;
use Exception;
use Illuminate\Support\Facades\Auth;
use Throwable;

class ModelException extends Exception
{
    protected $logMessage;
    protected $errorCode;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $logMessage = "", $errorCode = null)
    {
        parent::__construct($message, (int)$code, $previous);
        $this->logMessage = $logMessage;
        $this->errorCode  = $errorCode;
    }

    public function render()
    {
        return RestResponse::createResponseInternalServerError($this->getErrorCode());
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function report()
    {
        $context = [
            'exception'      => $this->getMessage(),
            'exception_file' => $this->getFile() . ':' . $this->getLine(),
            'trace'          => $this->getTraceAsString(),
            'postdata'       => request()->all(),
        ];

        if (Auth::check()) {
            $context['user_id'] = Auth::id();
        }
        app('log')->error($this->logMessage, $context);
    }
}
