<?php
/**
 * test - Order.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 14.33
 */

namespace App\Models;


use App\Helpers\Constants\DBTable;
use App\Traits\AuditUserAuto;
use App\Traits\SoftDeleteAuto;
use App\Traits\Uuid32Model;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property int $status 1->pending | 2->accepted | 3->processed
 * @property int $product_id
 * @property int $quantity
 * @property string $address_name
 * @property string $address_street
 * @property string $address_city
 * @property string $address_country
 * @property string $address_postcode
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddressCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddressCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddressName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddressPostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddressStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUuid($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    use Uuid32Model;
    use AuditUserAuto;
    use SoftDeleteAuto;

    protected $table = DBTable::ORDERS;

    protected $fillable = ['status', 'updated_by', 'update_at'];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
