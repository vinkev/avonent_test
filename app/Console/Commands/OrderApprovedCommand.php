<?php
/**
 * test - OrderApprovedCommand.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 17.27
 */

namespace App\Console\Commands;

use App\Exceptions\ModelException;
use App\Helpers\Constants\ErrorCodes;
use App\Helpers\Constants\OrderStatus;
use App\Models\Order;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class OrderApprovedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:approve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set order status into approved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws ModelException
     */
    public function handle()
    {
        try {
            Order::where('status', OrderStatus::ACCEPTED)
                ->whereDate('created_at', '<=', Carbon::yesterday()->toDateString())
                ->update([
                    'status'     => OrderStatus::PROCESSED,
                    'updated_by' => 0,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);

        } catch (Exception $e) {
            $this->error('DB Error on updating order status.');

            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating order status into approved', null);
        }

        $this->info("All accepted orders have been set into processed.");
    }
}
