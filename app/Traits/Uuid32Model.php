<?php
/**
 * test - Uuid32Trait.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 08.51
 */

namespace App\Traits;


use Ramsey\Uuid\Uuid;

trait Uuid32Model
{
    public static function bootUuid32Model()
    {
        static::creating(function ($model) {
            if (empty($model->uuid)) {
                $model->uuid = str_replace('-', '', Uuid::uuid4());
            }
        });
    }
}
