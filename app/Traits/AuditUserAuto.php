<?php
/**
 * test - AuditTrait.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 08.52
 */

namespace App\Traits;


use Illuminate\Support\Facades\Auth;

trait AuditUserAuto
{
    public static function bootAuditUserAuto()
    {
        static::creating(function ($model) {
            if (empty($model->created_by)) {
                $model->created_by = optional(Auth::user())->id ?? 0;
            }
        });

        static::updating(function ($model) {
            if (empty($model->updated_by)) {
                $model->updated_by = optional(Auth::user())->id ?? 0;
            }
        });
    }
}
