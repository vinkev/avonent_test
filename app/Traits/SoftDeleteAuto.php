<?php
/**
 * test - SoftDeleteAutoTrait.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 08.52
 */

namespace App\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

trait SoftDeleteAuto
{
    use SoftDeletes;

    public static function bootSoftDeleteAuto()
    {
        static::deleting(function ($model) {
            if (empty($model->deleted_by)) {
                $model->deleted_by = optional(Auth::user())->id ?? 0;
                $model->save();
            }
        });
    }
}
