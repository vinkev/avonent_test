<?php
/**
 * test - ForceJsonResponse.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 11.18
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

class ForceJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure                  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');

        return $next($request);
    }
}
