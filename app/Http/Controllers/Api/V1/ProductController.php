<?php
/**
 * test - ProductController.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 09.02
 */

namespace App\Http\Controllers\Api\V1;


use App\Exceptions\ModelException;
use App\Helpers\Classes\RestResponse;
use App\Helpers\Classes\RestResponseFractal;
use App\Helpers\Constants\ErrorCodes;
use App\Models\Product;
use App\Transformers\Api\V1\ProductTransformer;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Product::class);

        $products = Product::paginate(config('general.pagination'));

        return RestResponseFractal::createResponseSuccess($products, new ProductTransformer());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ValidationException
     * @throws ModelException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Product::class);

        $input = $this->validate($request, [
            'name'        => 'required|max:80',
            'description' => 'required',
            'stock'       => 'required|integer|min:0',
        ]);

        $product              = new Product();
        $product->name        = $input['name'];
        $product->description = $input['description'];
        $product->quantity    = $input['stock'];
        $product->is_stock    = $input['stock'] > 0;

        try {
            $product->save();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating new product', ErrorCodes::T201002);
        }

        return RestResponseFractal::createResponseSuccess($product, new ProductTransformer());
    }

    /**
     * @param Request $request
     * @param string  $productId
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $productId)
    {
        $product = Product::where('uuid', $productId)->firstOrFail();

        $this->authorize('view', $product);

        return RestResponseFractal::createResponseSuccess($product, new ProductTransformer());
    }

    /**
     * @param Request $request
     * @param string  $productId
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ModelException
     * @throws ValidationException
     */
    public function update(Request $request, $productId)
    {
        $product = Product::where('uuid', $productId)->firstOrFail();

        $this->authorize('update', $product);

        $input = $this->validate($request, [
            'name'        => 'required|max:80',
            'description' => 'required',
            'stock'       => 'required|integer|min:0',
        ]);

        $product->name        = $input['name'];
        $product->description = $input['description'];
        $product->quantity    = $input['stock'];
        $product->is_stock    = $input['stock'] > 0;

        try {
            $product->save();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating product', ErrorCodes::T201003);
        }

        return RestResponseFractal::createResponseSuccess($product, new ProductTransformer());
    }

    /**
     * @param Request $request
     * @param string  $productId
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ModelException
     */
    public function destroy(Request $request, $productId)
    {
        $product = Product::where('uuid', $productId)->firstOrFail();

        $this->authorize('delete', $product);

        try {
            $product->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting product', ErrorCodes::T201004);
        }

        return RestResponse::createResponseSuccess();
    }
}
