<?php
/**
 * test - OrderController.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 14.30
 */

namespace App\Http\Controllers\Api\V1;


use App\Exceptions\ModelException;
use App\Helpers\Classes\RestResponse;
use App\Helpers\Classes\RestResponseFractal;
use App\Helpers\Constants\DBTable;
use App\Helpers\Constants\ErrorCodes;
use App\Helpers\Constants\OrderStatus;
use App\Mail\OrderAcceptedMail;
use App\Models\Order;
use App\Models\Product;
use App\Transformers\Api\V1\OrderTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $filter = $this->validate($request, [
            'status' => 'nullable|in:' . implode(',', OrderStatus::getConstants()),
        ]);

        $query = Order::with(['product', 'user']);

        if (!Gate::allows('viewAll', Order::class)) {
            $query->where('user_id', $request->user()->id);
        }

        if (is_array($filter)) {
            if (!empty($filter['status'])) {
                $query->where('status', $filter['status']);
            }
        }

        $orders = $query->orderBy('created_at', 'DESC')
            ->paginate(config('general.pagination'));

        return RestResponseFractal::createResponseSuccess($orders, new OrderTransformer(), ['product', 'user']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ModelException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Order::class);

        $input = $this->validate($request, [
            'product_id'       => 'required|string',
            'quantity'         => 'required|integer|min:0',
            'address.name'     => 'required|string',
            'address.street'   => 'required|string',
            'address.city'     => 'required|string',
            'address.country'  => 'required|string',
            'address.postcode' => 'required|string',
        ]);

        $product = Product::where('uuid', $input['product_id'])->first();
        if (empty($product)) {
            return RestResponse::createResponseUnauthorize(ErrorCodes::T201001);
        }

        if ($product->quantity < $input['quantity']) {
            return RestResponse::createResponseUnauthorize(ErrorCodes::T201005);
        }

        $order                   = new Order();
        $order->user_id          = $request->user()->id;
        $order->product_id       = $product->id;
        $order->quantity         = $input['quantity'];
        $order->status           = OrderStatus::PENDING;
        $order->address_name     = $input['address']['name'];
        $order->address_street   = $input['address']['street'];
        $order->address_city     = $input['address']['city'];
        $order->address_country  = $input['address']['country'];
        $order->address_postcode = $input['address']['postcode'];

        try {
            $order->save();
            DB::table(DBTable::PRODUCTS)->where('id', $product->id)
                ->decrement('quantity', $input['quantity']);

        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating new order', ErrorCodes::T202002);
        }

        $order->load(['product', 'user']);

        return RestResponseFractal::createResponseSuccess($order, new OrderTransformer(), ['product', 'user']);
    }

    /**
     * @param Request $request
     * @param string  $orderId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Request $request, $orderId)
    {
        $order = Order::where('uuid', $orderId)->firstOrFail();

        $this->authorize('view', $order);

        $order->load(['product', 'user']);

        return RestResponseFractal::createResponseSuccess($order, new OrderTransformer(), ['product', 'user']);
    }

    /**
     * @param Request $request
     * @param string  $orderId
     * @return \Illuminate\Http\JsonResponse
     * @throws ModelException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $orderId)
    {
        $order = Order::where('uuid', $orderId)->firstorFail();

        $this->authorize('update', $order);

        $input = $this->validate($request, [
            'status' => 'required|in:' . implode(',', OrderStatus::getConstants()),
        ]);

        try {
            $order->status = $input['status'];
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating order', ErrorCodes::T202003);
        }

        $order->load(['product', 'user']);

        if ($input['status'] == OrderStatus::ACCEPTED) {
            Mail::to($order->user->email)
                ->queue(new OrderAcceptedMail($order));
        }

        return RestResponseFractal::createResponseSuccess($order, new OrderTransformer(), ['product', 'user']);
    }

    /**
     * @param Request $request
     * @param string  $orderId
     * @return \Illuminate\Http\JsonResponse
     * @throws ModelException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request, $orderId)
    {
        $order = Order::where('uuid', $orderId)->firstorFail();

        $this->authorize('delete', $order);

        try {
            $order->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting order', ErrorCodes::T202004);
        }

        return RestResponse::createResponseSuccess();
    }
}
