<?php
/**
 * test - ProductPolicy.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 10.27
 */

namespace App\Policies;


use App\Helpers\Constants\UserType;
use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return in_array($user->type, UserType::getConstants());
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function view(User $user, Product $product)
    {
        return in_array($user->type, UserType::getConstants());
    }

    /**
     * @param User $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->type === UserType::ADMINISTRATOR;
    }

    /**
     * @param User $user
     * @param Product     $product
     * @return bool
     */
    public function update(User $user, Product $product)
    {
        return $user->type === UserType::ADMINISTRATOR;
    }

    /**
     * @param User $user
     * @param Product     $product
     * @return bool
     */
    public function delete(User $user, Product $product)
    {
        return $user->type === UserType::ADMINISTRATOR;
    }
}
