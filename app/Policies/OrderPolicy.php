<?php
/**
 * test - OrderPolicy.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 14.49
 */

namespace App\Policies;


use App\Helpers\Constants\UserType;
use App\Models\Order;
use App\Models\User;

class OrderPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function viewAll(User $user)
    {
        return $user->type === UserType::ADMINISTRATOR;
    }

    /**
     * @param User  $user
     * @param Order $order
     * @return bool
     */
    public function view(User $user, Order $order)
    {
        return ($user->type === UserType::SITE_API && $order->user_id = $user->id) || $user->type === UserType::ADMINISTRATOR;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->type === UserType::SITE_API;
    }

    /**
     * @param User  $user
     * @param Order $order
     * @return bool
     */
    public function update(User $user, Order $order)
    {
        return $user->type === UserType::ADMINISTRATOR;
    }

    /**
     * @param User  $user
     * @param Order $order
     * @return bool
     */
    public function delete(User $user, Order $order)
    {
        return ($user->type === UserType::SITE_API && $order->user_id = $user->id) || $user->type === UserType::ADMINISTRATOR;
    }
}
