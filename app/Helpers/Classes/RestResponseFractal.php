<?php
/**
 * test - RestResponse.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 09.42
 */

namespace App\Helpers\Classes;


use App\Helpers\Constants\ErrorCodes;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use stdClass;

class RestResponseFractal
{
    public $error_code;
    public $errors;
    public $data;
    public $meta;

    public function __construct()
    {
        $this->meta   = new stdClass();
    }

    /**
     * Response 200
     *
     * @param Model|Collection|LengthAwarePaginator $data
     * @param TransformerAbstract                   $transformer
     * @param array                                 $includes
     * @param array                                 $headers
     * @return JsonResponse
     */
    public static function createResponseSuccess($data, $transformer = null, $includes = [], $headers = [])
    {
        $response = new RestResponseFractal();

        $data = fractal($data, $transformer)->parseIncludes($includes)->toArray();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->success($data)
            , 200
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    private function success($data)
    {
        unset($this->errors);

        $this->meta = $data['meta'] ?? new stdClass();
        unset($data['meta']);

        $this->data = count($data) ? $data : new stdClass();

        $this->error_code = ErrorCodes::T000000;

        return $this;
    }
}
