<?php
/**
 * test - RestResponse.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 11.27
 */

namespace App\Helpers\Classes;


use App\Helpers\Constants\ErrorCodes;
use Illuminate\Http\JsonResponse;
use stdClass;

class RestResponse
{
    public $error_code;
    public $errors;
    public $data;

    public function __construct()
    {
    }

    /**
     * Response 200
     *
     * @param null  $data
     * @param array $headers
     * @return JsonResponse
     */
    public static function createResponseSuccess($data = null, $headers = [])
    {
        $response = new RestResponse();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->success($data)
            , 200
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    private function success($data)
    {

        unset($this->errors);

        if ($data) {

            $this->data = $data;

        } else {

            $this->data = new stdClass();
        }

        $this->error_code = ErrorCodes::T000000;

        return $this;
    }

    /**
     * Response 400
     *
     * @param string $error_code
     * @param array  $errors
     * @return JsonResponse
     */
    public static function createResponseBadRequest($error_code, $errors = [])
    {

        $response = new RestResponse();

        return response()->json(
            $response->error($error_code, $errors)
            , 400
            , ['Content-Type' => 'application/json; charset=utf-8']
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    private function error($error_code, $errors)
    {

        unset($this->data);

        $this->error_code = $error_code;

        if ($errors) {

            $this->errors = $errors;

        } else {

            $this->errors = [];
        }

        return $this;
    }

    /**
     * Response 401
     *
     * @param string $error_code
     * @param array  $errors
     * @param array  $headers
     * @return JsonResponse
     */
    public static function createResponseUnauthorize($error_code, $errors = [], $headers = [])
    {

        $response = new RestResponse();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->error($error_code, $errors)
            , 401
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 403
     *
     * @param string $error_code
     * @param array  $errors
     * @return JsonResponse
     */
    public static function createResponseAccessDenied($error_code, $errors = [])
    {

        $response = new RestResponse();

        return response()->json(
            $response->error($error_code, $errors)
            , 403
            , ['Content-Type' => 'application/json; charset=utf-8']
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 404
     *
     * @param string $error_code
     * @param array  $errors
     * @param array  $headers
     * @return JsonResponse
     */
    public static function createResponseNotFound($error_code, $errors = [], $headers = [])
    {

        $response = new RestResponse();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->error($error_code, $errors)
            , 404
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 422
     *
     * @param       $error_code
     * @param array $errors
     * @return JsonResponse
     */
    public static function createResponseUnprocessableEntity($error_code, $errors = [])
    {

        $response = new RestResponse();

        return response()->json(
            $response->error($error_code, $errors)
            , 422
            , ['Content-Type' => 'application/json; charset=utf-8']
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 429
     *
     * @param array $headers
     * @param array $errors
     * @return JsonResponse
     */
    public static function createResponseTooManyRequests($headers = [], $errors = [])
    {
        $response = new RestResponse();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->error(ErrorCodes::T200005, $errors)
            , 429
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 500
     *
     * @param string $error_code
     * @param array  $errors
     * @param array  $headers
     * @return JsonResponse
     */
    public static function createResponseInternalServerError($error_code, $errors = [], $headers = [])
    {

        $response = new RestResponse();

        $headers['Content-Type'] = 'application/json; charset=utf-8';

        return response()->json(
            $response->exception($error_code, $errors)
            , 500
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    private function exception($error_code, $errors = null)
    {

        unset($this->data);

        if (empty($errors)) {

            unset($this->errors);

        } else {

            $this->errors = $errors;
        }

        $this->error_code = $error_code;

        return $this;
    }

    /**
     * Response 503
     *
     * @param string $errors
     * @return JsonResponse
     */
    public static function createResponseMaintenanceMode($errors = null)
    {

        $response = new RestResponse();

        $headers = [
            'Content-Type' => 'application/json; charset=utf-8',
        ];

        return response()->json(
            $response->error(ErrorCodes::T200006, $errors)
            , 503
            , $headers
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

}
