<?php
/**
 * test - MigrationHelper.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 08.50
 */

namespace App\Helpers\Classes;


use Illuminate\Database\Schema\Blueprint;

class MigrationHelper
{
    /**
     * @param Blueprint $table
     * @param bool      $with_user_audit
     */
    public static function audit($table, $with_user_audit = true)
    {
        if ($with_user_audit) {
            $table->bigInteger('created_by', false, true)->default(0);
            $table->bigInteger('updated_by', false, true)->nullable()->default(null);
            $table->bigInteger('deleted_by', false, true)->nullable()->default(null);
        }
        $table->timestampTz('created_at')->useCurrent();
        $table->timestampTz('updated_at')->nullable()->default(null);
        $table->timestampTz('deleted_at')->nullable()->default(null);
    }
}
