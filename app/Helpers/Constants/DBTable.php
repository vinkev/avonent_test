<?php
/**
 * test - DBTable.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 08.42
 */

namespace App\Helpers\Constants;


use App\Helpers\Abstracts\Constant;

class DBTable extends Constant
{
    const PRODUCTS = 'products';
    const ORDERS   = 'orders';

    const USERS = 'users';
}
