<?php
/**
 * test - UserType.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 15.18
 */

namespace App\Helpers\Constants;


use App\Helpers\Abstracts\Constant;

class UserType extends Constant
{
    const ADMINISTRATOR = 'Administrator';
    const SITE_API      = 'Site API';
}
