<?php
/**
 * test - OrderStatus.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 14.57
 */

namespace App\Helpers\Constants;


use App\Helpers\Abstracts\Constant;

class OrderStatus extends Constant
{
    const PENDING   = '1';
    const ACCEPTED  = '2';
    const PROCESSED = '3';
}
