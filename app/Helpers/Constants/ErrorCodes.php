<?php
/**
 * test - ErrorCodes.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 11.22
 */

namespace App\Helpers\Constants;


class ErrorCodes
{
    /**
     * Success
     */
    const T000000 = '000000';


    /* ****************************
     * General
     * ****************************/
    /**
     * Unknown server error
     */
    const T100001 = '100001';


    /* ****************************
     * API
     * ****************************/

    /**
     * Validation failed
     */
    const T200001 = '200001';
    /**
     * Access denied
     */
    const T200002 = '200002';
    /**
     * Access unauthorized
     */
    const T200003 = '200003';
    /**
     * Model not found
     */
    const T200004 = '200004';
    /**
     * Too many request
     */
    const T200005 = '200005';
    /**
     * Maintenance mode
     */
    const T200006 = '200006';


    /*
     * Product
     */
    /**
     * Product not found
     */
    const T201001 = '201001';
    /**
     * Error on creating product
     */
    const T201002 = '201002';
    /**
     * Error on updating product
     */
    const T201003 = '201003';
    /**
     * Error on deleting product
     */
    const T201004 = '201004';
    /**
     * Product out of stock
     */
    const T201005 = '201005';



    /*
     * Order
     */
    /**
     * Order not found
     */
    const T202001 = '202001';
    /**
     * Error on creating order
     */
    const T202002 = '202002';
    /**
     * Error on updating order
     */
    const T202003 = '202003';
    /**
     * Error on deleting order
     */
    const T202004 = '202004';
}
