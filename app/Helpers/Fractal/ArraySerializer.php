<?php
/**
 * test - ArraySerializer.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 16.50
 */

namespace App\Helpers\Fractal;

use League\Fractal\Serializer\ArraySerializer as FractalArraySerializer;

class ArraySerializer extends FractalArraySerializer
{
    /**
     * Serialize a collection to a plain array.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        return $data;
    }

    /**
     * Serialize an item to a null or plain array.
     *
     * @param string $resourceKey
     * @param array $data
     * @return array|null
     */
    public function item($resourceKey, array $data)
    {
        if (empty($data)) {
            return null;
        }

        return $data;
    }
}
