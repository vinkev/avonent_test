<?php
/**
 * test - ProjectTransformer.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 09.50
 */

namespace App\Transformers\Api\V1;


use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id'          => $product->uuid,
            'name'        => $product->name,
            'description' => $product->description,
            'quantity'    => (int)$product->quantity,
            'is_stock'    => $product->is_stock,
        ];
    }
}
