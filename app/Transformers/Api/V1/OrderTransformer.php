<?php
/**
 * test - OrderTransformer.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 14.36
 */

namespace App\Transformers\Api\V1;


use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'product'];

    public function transform(Order $order)
    {
        return [
            'id'         => $order->uuid,
            'status'     => (int) $order->status,
            'quantity'   => (int) $order->quantity,
            'address'    => [
                'name'     => $order->address_name,
                'street'   => $order->address_street,
                'city'     => $order->address_city,
                'country'  => $order->address_country,
                'postcode' => $order->address_postcode,
            ],
            'created_at' => $order->created_at->timestamp,
        ];
    }

    public function includeUser(Order $order)
    {
        $user = $order->user;

        return $this->item($user, new UserTransformer);
    }

    public function includeProduct(Order $order)
    {
        $product = $order->product;

        return $this->item($product, new ProductTransformer());
    }
}
