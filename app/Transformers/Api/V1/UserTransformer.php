<?php
/**
 * test - UserTransformer.php
 *
 * @author: Kevin L.
 * Date: 03/05/20
 * Time: 18.22
 */

namespace App\Transformers\Api\V1;


use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'   => $user->uuid,
            'name' => $user->name,
        ];
    }
}
