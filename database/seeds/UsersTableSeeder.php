<?php

use App\Helpers\Constants\UserType;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user            = new User();
        $user->name      = 'API user';
        $user->email     = 'user@user.com';
        $user->api_token = '8a5da52ed126447d359e70c05721a8aa';
        $user->type      = UserType::SITE_API;
        $user->save();

        $user            = new User();
        $user->name      = 'Admin';
        $user->email     = 'admin@admin.com';
        $user->api_token = '21232f297a57a5a743894a0e4a801fc3';
        $user->type      = UserType::ADMINISTRATOR;
        $user->save();
    }
}
