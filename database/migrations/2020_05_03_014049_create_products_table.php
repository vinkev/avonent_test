<?php

use App\Helpers\Classes\MigrationHelper;
use App\Helpers\Constants\DBTable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DBTable::PRODUCTS, function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('name', 80);
            $table->text('description')->nullable()->default(null);
            $table->integer('quantity', false, true)->default(0);
            $table->boolean('is_stock')->default(false);

            MigrationHelper::audit($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::PRODUCTS);
    }
}
