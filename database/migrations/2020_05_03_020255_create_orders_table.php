<?php

use App\Helpers\Classes\MigrationHelper;
use App\Helpers\Constants\DBTable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DBTable::ORDERS, function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->bigInteger('user_id', false, true);
            $table->tinyInteger('status', false, true)->comment('1->pending | 2->accepted | 3->processed');
            $table->bigInteger('product_id', false, true);
            $table->integer('quantity', false, true);
            $table->string('address_name');
            $table->string('address_street');
            $table->string('address_city');
            $table->string('address_country');
            $table->string('address_postcode');

            MigrationHelper::audit($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::ORDERS);
    }
}
